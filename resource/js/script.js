const container = document.querySelector(".container");
const seats = document.querySelectorAll(".row .seat:not(.occupied)");
const count = document.getElementById("count");
const total = document.getElementById("total");
const price = document.getElementById("price");
const movieSelect = document.getElementById("movie");
var A = 170;
var B = 160;
var C = 150;
var D = 190; 

function prices(){
  if(movieSelect.value == "Adan"){
     document.getElementById("price").setAttribute("value",D);
  }else if(movieSelect.value == "Hibla"){
    document.getElementById("price").setAttribute("value",B);
  }else if(movieSelect.value == "Silip"){
    document.getElementById("price").setAttribute("value",C);
  }else if(movieSelect.value == "Patikim ng Pinya"){
    document.getElementById("price").setAttribute("value",A);
  }
}


function updateSelectedCount() {
  if(movieSelect.value == "Adan"){
  const selectedSeats = document.querySelectorAll(".row .selected");
  const selectedSeatsCount = selectedSeats.length;
  count.innerHTML = selectedSeatsCount;
  total.innerHTML =  selectedSeatsCount * D;
  }else if(movieSelect.value == "Hibla"){
    const selectedSeats = document.querySelectorAll(".row .selected");
  const selectedSeatsCount = selectedSeats.length;
  count.innerHTML = selectedSeatsCount;
  total.innerHTML = selectedSeatsCount * B;
  }else if(movieSelect.value == "Silip"){
    const selectedSeats = document.querySelectorAll(".row .selected");
  const selectedSeatsCount = selectedSeats.length;
  count.innerHTML = selectedSeatsCount;
  total.innerHTML = selectedSeatsCount * C;
  }else if(movieSelect.value == "Patikim ng Pinya"){
    const selectedSeats = document.querySelectorAll(".row .selected");
  const selectedSeatsCount = selectedSeats.length;
  count.innerHTML = selectedSeatsCount;
  total.innerHTML = selectedSeatsCount * A;
  }
}

movieSelect.addEventListener("change", (e) => {
  ticketPrice = parseInt(e.target.value);
  updateSelectedCount();
  prices();
});

container.addEventListener("dblclick", (e) => {
  console.log(e);
  if (
    e.target.classList.contains("seat") &&
    !e.target.classList.contains("occupied")
  ) {
    e.target.classList.toggle("selected");
    updateSelectedCount();
  }
});

function test(){
const divs = document.querySelectorAll('.container');
divs.forEach(el => el.addEventListener('dblclick', event => {
  v = event.target.getAttribute("value");
  
  document.getElementById("s1").setAttribute("value",v);
}));
}




